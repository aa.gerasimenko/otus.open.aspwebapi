using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace asp_simple_service.Controllers;

[ApiController]
[Route("[controller]")]
public class OperationController : ControllerBase
{
    private readonly IRepository<Operation> _repository;
    private readonly ILogger<OperationController> _logger;

    public OperationController(ILogger<OperationController> logger,
        IRepository<Operation> repository
    )
    {
        _repository = repository;
        _logger = logger;
    }

    [HttpGet]
    public async Task<IEnumerable<Operation>> Get()
    {
        return await _repository.GetAllAsync();
    }

    [HttpPost]
    public async Task<Operation> Post([FromBody] Operation entity)
    {
        entity.Id = Guid.NewGuid();
        await _repository.AddAsync(entity);
        return entity;
    }
}
