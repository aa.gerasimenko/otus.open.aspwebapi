﻿// See https://aka.ms/new-console-template for more information
using Spectre.Console;


var _client = new SimpleHttpClient("http://localhost:5126");

var c = "";
while (c != "exit")
{
    Console.Clear();
    var ch = AnsiConsole.Prompt(
      new SelectionPrompt<Choise>()
          .AddChoices(new[] {
          new Choise("all", "Get all todos"),
          new Choise("new", "Create"),
          new Choise("exit", "Exit"),
    }));

    c = ch.Key;

    try
    {
        switch (c)
        {
            case "all":
                await ShowOperations();
                break;
            case "new":
                await CreateNewTodo();
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        AnsiConsole.Write(new Markup("[bold red]Error[/]") + ex.Message);
        Console.ReadLine();
    }

}

async Task CreateNewTodo()
{
    var operation = new Operation
    {
        Id = Guid.NewGuid(),
        CashId = Guid.Empty,
        CategoryId = Guid.Empty,
        Date = DateTime.UtcNow,
        Description = AnsiConsole.Ask<string>("Input [green]description[/]: "),
        Value = AnsiConsole.Ask<int>("Input [green]value[/]: "),
    };

    var response = await _client.PostAsync<Operation>("/operation", operation);
    Console.ReadLine();
}

async Task ShowOperations()
{
    var response = await _client.GetAsync<IEnumerable<Operation>>("/operation");

    var table = new Table();

    // Add some columns
    table.AddColumn("Date");
    table.AddColumn(new TableColumn("Description").Centered());
    table.AddColumn(new TableColumn("Value").Centered());

    foreach (var row in response)
    {
        // Add some rows
        table.AddRow(row.Date.ToString(""), row.Description, row.Value.ToString());
    }
    // Render the table to the console
    AnsiConsole.Write(table);

    Console.ReadLine();
}
