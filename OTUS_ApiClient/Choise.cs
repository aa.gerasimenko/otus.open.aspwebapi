// See https://aka.ms/new-console-template for more information
class Choise
{
  public Choise(string key, string title)
  {
    Key = key;
    Title = title;
  }
  public string Key { get; }
  public string Title { get; }

  public override string ToString()
  {
    return Title;
  }
}