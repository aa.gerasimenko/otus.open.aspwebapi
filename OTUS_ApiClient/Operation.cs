public class Operation
{
    public Guid Id { get; set; }
    public Guid CategoryId { get; set; }
    public Guid CashId { get; set; }
    public DateTime Date { get; set; }
    public string Description { get; set; }
    public int Value { get; set; }
}