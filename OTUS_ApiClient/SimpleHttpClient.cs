// See https://aka.ms/new-console-template for more information
using System.Net.Http.Json;

class SimpleHttpClient
{
    private string _baseurl;

    public SimpleHttpClient(string baseurl)
    {
        _baseurl = baseurl;
    }

    public async Task<string> GetContent(string url)
    {
        HttpClient httpClient = GetClient();

        using var response = await httpClient.GetAsync(url);
        return await response.Content.ReadAsStringAsync();
    }

    public async Task<T> GetAsync<T>(string url)
    {
        HttpClient httpClient = GetClient();

        using var response = await httpClient.GetAsync(url);
        return await response.Content.ReadFromJsonAsync<T>();
    }

    public async Task<T> PostAsync<T>(string url, T op)
    {
        HttpClient httpClient = GetClient();

        JsonContent content = JsonContent.Create(op);
        using var response = await httpClient.PostAsync(url, content);
        return await response.Content.ReadFromJsonAsync<T>();
    }

    private HttpClient GetClient()
    {
        return new HttpClient()
        {
            BaseAddress = new Uri(_baseurl)
        };
    }
}