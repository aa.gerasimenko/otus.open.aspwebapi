using Microsoft.EntityFrameworkCore;

namespace asp_simple_service;

public class DataContext : DbContext
{
    public DbSet<Operation> Operations { get; set; }

    public DataContext()
    {
    }

    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }
}
