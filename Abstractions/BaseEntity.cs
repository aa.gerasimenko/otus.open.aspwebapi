namespace asp_simple_service;

public class BaseEntity
{
    public Guid Id { get; set; }
}